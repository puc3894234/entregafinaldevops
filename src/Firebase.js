import firebase from "firebase/app";
import 'firebase/firestore';
import 'firebase/auth';

const firebaseConfig = {
    apiKey: "AIzaSyCmbxIfRABuFuK-K1OGYdHkRk3t8sDFviM",
    authDomain: "projetoead-6f3df.firebaseapp.com",
    projectId: "projetoead-6f3df",
    storageBucket: "projetoead-6f3df.appspot.com",
    messagingSenderId: "1067198518244",
    appId: "1:1067198518244:web:b5aa5ab2eded7a8cbe2df1"
  };

if(!firebase.apps.lenght){
    firebase.initializeApp(firebaseConfig);
}

export default firebase;