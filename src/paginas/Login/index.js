import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import firebase from '../../Firebase';
import '../../styles.css';

class Login extends Component{
    constructor(props){
      super(props);
      this.state = {
        email: "",
        senha: "",
        erro: null, 
      }

      this.acessar = this.acessar.bind(this);
    }

    async acessar(){

      await firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.senha)
      .then(()=>{
        window.location.href = "./principal";
      })
      .catch((erro)=>{
        this.setState({ erro: "Credenciais inválidas. Verifique seu e-mail e senha." });

      });

    }

    render(){
      return(
        <div className="login-form">
        <h1> Página de Login</h1>
        <input className="login-input" type="text" placeholder='E-mail' onChange={(e) => this.setState({ email: e.target.value })} />
        <br />
        <input className="login-input" type="password" placeholder='Senha' onChange={(e) => this.setState({ senha: e.target.value })} />
        <br />
        <button className="login-button" onClick={this.acessar}>Acessar</button>
        <br />
        {this.state.erro && <p className="error-message">{this.state.erro}</p>} 
        <br />
        <Link className="link-to-signup" to="/cadastro">Ir para a página de Cadastro</Link> 
      </div>
      )
    }
}

export default Login;