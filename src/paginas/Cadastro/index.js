import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import firebase from '../../Firebase';
import '../../styles.css';

class Cadastro extends Component {
    constructor(props){
        super(props);
        this.state = {
            email: "",
            senha: "",
            nome: "",
            sobrenome: "",
            data_nascimento: ""
        }

        this.gravar = this.gravar.bind(this);
    }

    async gravar(){
        
        await firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.senha)
        .then( async (retorno) => {

            await firebase.firestore().collection("usuario").doc(retorno.user.uid).set({
                nome: this.state.nome,
                sobrenome: this.state.sobrenome,
                data_nascimento: this.state.data_nascimento
            });
        });

       

        /*firebase.firestore().collection("usuario").add({
            nome: this.state.nome,
            sobrenome: this.state.sobrenome
        });*/

        /*await firebase.firestore().collection("usuario").doc("1").set({
            nome: this.state.nome,
            sobrenome: this.state.sobrenome
        });*/
    }

    render(){
        return(
            <div>
                <h1> Página de Cadastro</h1>
                <input type="text" className="login-input" placeholder='E-mail' onChange={(e) => this.setState({email: e.target.value})} />
                <br/>
                <input type="password" className="login-input" placeholder='Senha' onChange={(e) => this.setState({senha: e.target.value})} />
                <br/>
                <input type="text" className="login-input" placeholder='Nome' onChange={(e) => this.setState({nome: e.target.value})} />
                <br/>
                <input type="text" className="login-input" placeholder='Sobrenome' onChange={(e) => this.setState({sobrenome: e.target.value})} />
                <br/>
                <input type="date" className="login-input" placeholder='Data de Nascimento' onChange={(e) => this.setState({data_nascimento: e.target.value})} />
                <br/>
                <button className="login-button" onClick={this.gravar}> Gravar</button>
                <div>
                <br />
                <Link to="/login">Ir para a página de login</Link>
            </div>

            </div>
        )
    }
}

export default Cadastro;