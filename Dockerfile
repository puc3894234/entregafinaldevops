# Use uma imagem de node.js como base
FROM node:14

# Crie um diretório de trabalho dentro do contêiner
WORKDIR /app

# Copie o arquivo package.json e o arquivo package-lock.json (se existir)
COPY package*.json ./

# Instale as dependências do Node.js
RUN npm install

# Copie todos os arquivos do diretório local para o diretório de trabalho no contêiner
COPY . .

# Construa a aplicação React
RUN npm run build

# Exponha a porta 80 para o mundo exterior
EXPOSE 80

# Inicie a aplicação quando o contêiner for executado
CMD [ "npm", "start" ]
